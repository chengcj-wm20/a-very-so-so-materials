// C# code
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Prac1
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            // auto focus on the text field
            txtName.Focus();
            // if focus is not called, then user have to manually click on the text field to focus on it
        }
        
        // text change event
        protected void TxtNameCopy_TextChanged(object sender, EventArgs e)
        {
            // Text changed is trigger when txtName's (text is changed + txtName lost focus)
            // set (2) AutoPostBack to true: (2) will re-send the text to the server and trigger the text change on (1)
            // (1) = txtNameCopy; (2) = txtName
            txtNameCopy.Text = txtName.Text;
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            lblMessage.Text = txtName.Text;
        }
    }
}