================================================================
SELECT * 
FROM (SELECT OrderNumber, orderDate, customerNumber
      FROM Orders
      ORDER BY OrderDate DESC)
WHERE ROWNUM <=10;

UPDATE Orders
SET orderDate = ADD_MONTHS(orderDate, 213),
    requiredDate = ADD_MONTHS(requiredDate, 213),
    shippedDate = ADD_MONTHS(shippedDate, 213);

=================================================================
-- 1. IF-ELSE-END IF statement
CREATE OR REPLACE PROCEDURE prc_last_order (v_custNo IN NUMBER) IS
 v_orderDate DATE;
 v_duration NUMBER(4);
 v_months NUMBER(4,1);

BEGIN
 SELECT MAX(orderDate) INTO v_orderDate
 FROM orders
 WHERE customerNumber = v_custNo; 

IF (v_orderDate IS NOT NULL) THEN
 DBMS_OUTPUT.PUT_LINE('Customer: ' || v_custNo || ' last made an order on ' || v_orderDate);
 
 v_duration := ROUND (SYSDATE - v_orderDate, 0);

 IF (v_duration < 30) THEN
     DBMS_OUTPUT.PUT_LINE('That was ' || v_duration || ' days ago.');
 ELSE
  v_months := MONTHS_BETWEEN(SYSDATE, v_orderDate);
  DBMS_OUTPUT.PUT_LINE('That was ' || v_months || ' months ago.');
 END IF;
ELSE
 DBMS_OUTPUT.PUT_LINE('Customer: ' || v_custNo || ' is not found.');
END IF;
END;
/

-- 2. IF-ELSE-END IF statement with RAISE_APPLICATION_ERROR
CREATE OR REPLACE PROCEDURE prc_last_order (v_custNo IN NUMBER) IS
 v_orderDate DATE;
 v_duration NUMBER(4);
 v_months NUMBER(4,1);

BEGIN
 SELECT MAX(orderDate) INTO v_orderDate
 FROM orders
 WHERE customerNumber = v_custNo; 

IF (v_orderDate IS NOT NULL) THEN
 DBMS_OUTPUT.PUT_LINE('Customer: ' || v_custNo || ' last made an order on ' || v_orderDate);
 
 v_duration := ROUND (SYSDATE - v_orderDate, 0);

 IF (v_duration < 30) THEN
     DBMS_OUTPUT.PUT_LINE('That was ' || v_duration || ' days ago.');
 ELSE
  v_months := MONTHS_BETWEEN(SYSDATE, v_orderDate);
  DBMS_OUTPUT.PUT_LINE('That was ' || v_months || ' months ago.');
 END IF;
ELSE
 RAISE_APPLICATION_ERROR (-20000, 'Customer: ' || v_custNo || ' is not found.');
END IF;
END;
/

-- 3. IF-ELSE-END IF statement with default exception
CREATE OR REPLACE PROCEDURE prc_last_order (v_custNo IN NUMBER) IS
 v_orderDate DATE;
 v_duration NUMBER(4);
 v_months NUMBER(4,1);

BEGIN
 SELECT MAX(orderDate) INTO v_orderDate
 FROM orders
 WHERE customerNumber = v_custNo; 

IF (v_orderDate IS NOT NULL) THEN
 DBMS_OUTPUT.PUT_LINE('Customer: ' || v_custNo || ' last made an order on ' || v_orderDate);
 
 v_duration := ROUND (SYSDATE - v_orderDate, 0);

 IF (v_duration < 30) THEN
     DBMS_OUTPUT.PUT_LINE('That was ' || v_duration || ' days ago.');
 ELSE
  v_months := MONTHS_BETWEEN(SYSDATE, v_orderDate);
  DBMS_OUTPUT.PUT_LINE('That was ' || v_months || ' months ago.');
 END IF;
ELSE
 RAISE NO_DATA_FOUND;
END IF;

EXCEPTION
WHEN NO_DATA_FOUND THEN
  DBMS_OUTPUT.PUT_LINE('Customer: ' || v_custNo || ' is not found.');
END;
/

-- 4. IF-ELSE-END IF statement with custom exception
CREATE OR REPLACE PROCEDURE prc_last_order (v_custNo IN NUMBER) IS
 empty_data EXCEPTION;
 v_orderDate DATE;
 v_duration NUMBER(4);
 v_months NUMBER(4,1);

BEGIN
 SELECT MAX(orderDate) INTO v_orderDate
 FROM orders
 WHERE customerNumber = v_custNo; 

IF (v_orderDate IS NOT NULL) THEN
 DBMS_OUTPUT.PUT_LINE('Customer: ' || v_custNo || ' last made an order on ' || v_orderDate);
 
 v_duration := ROUND (SYSDATE - v_orderDate, 0);

 IF (v_duration < 30) THEN
     DBMS_OUTPUT.PUT_LINE('That was ' || v_duration || ' days ago.');
 ELSE
  v_months := MONTHS_BETWEEN(SYSDATE, v_orderDate);
  DBMS_OUTPUT.PUT_LINE('That was ' || v_months || ' months ago.');
 END IF;
ELSE
 RAISE empty_data;
END IF;

EXCEPTION
WHEN empty_data THEN
  DBMS_OUTPUT.PUT_LINE('Customer: ' || v_custNo || ' is not found.');
END;
/

EXEC prc_last_order(382)
EXEC prc_last_order(166)
EXEC prc_last_order(111)