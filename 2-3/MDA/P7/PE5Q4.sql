CREATE TABLE Products_Audit(
    UserID      VARCHAR(30),
    TransDate   DATE,
    Time        CHAR(8),
    Action      VARCHAR(10)
);

CREATE OR REPLACE TRIGGER trg_track_Products
AFTER INSERT OR UPDATE OR DELETE ON Products
DECLARE
timeString CHAR(8);

BEGIN
    CASE
        WHEN INSERTING THEN
            timeString := TO_CHAR(SYSDATE, 'HH24:MI:SS');
            INSERT INTO Products_Audit
            VALUES(USER, SYSDATE, timeString, 'INSERT');
        WHEN UPDATING THEN
            timeString := TO_CHAR(SYSDATE, 'HH24:MI:SS');
            INSERT INTO Products_Audit
            VALUES(USER, SYSDATE, timeString, 'UPDATE');
        WHEN DELETING THEN
            timeString := TO_CHAR(SYSDATE, 'HH24:MI:SS');
            INSERT INTO Products_Audit
            VALUES(USER, SYSDATE, timeString, 'DELETE');
    END CASE;
END;
/